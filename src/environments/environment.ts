import { LoggingServiceConfiguration } from 'ionic-logging-service';

const loggerConfig: LoggingServiceConfiguration = {
  logLevels: [
    {
      loggerName: 'root',
      logLevel: 'ALL',
    },
  ],
  browserConsoleAppender: { threshold: 'ALL' },
};

export const environment = {
  production: false,
  logging: loggerConfig,
};
