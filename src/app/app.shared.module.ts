import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgxFileHelpersModule } from 'ngx-file-helpers';
import { ProjectModalComponent } from './components/project-modal/project-modal.component';
import { SettingComponent } from './components/setting/setting.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { PipesModule } from './pipes/pipes.module';

@NgModule({
  imports: [RouterModule, IonicModule, CommonModule, FormsModule, PipesModule, NgxFileHelpersModule],
  declarations: [SideMenuComponent, SettingComponent, ProjectModalComponent],
  exports: [SideMenuComponent, SettingComponent, ProjectModalComponent, NgxFileHelpersModule],
})
export class AppSharedModule {}
