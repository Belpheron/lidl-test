import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'daily',
    pathMatch: 'full',
  },
  {
    path: 'projects',
    loadChildren: () => import('./pages/projects/projects.module').then((m) => m.ProjectsPageModule),
  },
  {
    path: 'projects/edit/:id',
    loadChildren: () => import('./pages/project-edit/project-edit.module').then((m) => m.ProjectEditPageModule),
  },
  {
    path: 'daily',
    loadChildren: () => import('./pages/daily-view/daily-view.module').then((m) => m.DailyViewPageModule),
  },
  {
    path: 'daily/logs/:logId/project-logs/:projectId',
    loadChildren: () => import('./pages/time-edit/time-edit.module').then((m) => m.TimeEditPageModule),
  },
  {
    path: 'daily/settings',
    loadChildren: () => import('./pages/personal-settings/personal-settings.module').then((m) => m.PersonalSettingsPageModule),
  },
  {
    path: 'daily/weeks',
    loadChildren: () => import('./pages/weekly-view/weekly-view.module').then((m) => m.WeeklyViewPageModule),
  },
  {
    path: '*',
    redirectTo: 'daily',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
