import { Pipe, PipeTransform } from '@angular/core';
import { Project } from '../dto/project';

@Pipe({
  name: 'skipDisabled',
})
export class SkipDisabledPipe implements PipeTransform {
  transform(projects: Project[]): Project[] {
    return projects == null ? null : projects.filter((p) => !p.disabled);
  }
}
