import { NgModule } from '@angular/core';
import { SkipDisabledPipe } from './skip-disabled.pipe';

@NgModule({
  declarations: [SkipDisabledPipe],
  exports: [SkipDisabledPipe],
})
export class PipesModule {}
