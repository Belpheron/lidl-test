import { Project } from './project';

export class DailyFormModel {
  selectedProject?: Project = {};
  timeToAdd? = 1;
  selectedDate?: string;
}
