import { DailyLog } from './daily-log';
import { Project } from './project';
import { Settings } from './settings';

export class Store {
  dailyLogs: DailyLog[];
  projects: Project[];
  settings: Settings;
}
