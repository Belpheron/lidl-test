export class ProjectLog {
  id?: string;
  projectId: string;
  projectName: string;
  amount: number;
}
