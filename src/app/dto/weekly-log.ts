import { ProjectLog } from './project-log';

export class WeeklyLog {
  start: string;
  end: string;
  logs: ProjectLog[] = [];
  sum = 0;
}
