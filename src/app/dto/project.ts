export class Project {
  id?: string;
  name?: string;
  toolName?: string;
  disabled?: boolean;
}
