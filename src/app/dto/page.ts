export class Page {
  title: string;
  url: string;
  icon: string;
}
