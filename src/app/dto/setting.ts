export class Setting {
  day: string;
  arrive?: string;
  leave?: string;
  pause?: number;
}
