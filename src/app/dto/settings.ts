import { Setting } from './setting';

export class Settings {
  monday: Setting;
  tuesday: Setting;
  wednesday: Setting;
  thursday: Setting;
  friday: Setting;
}
