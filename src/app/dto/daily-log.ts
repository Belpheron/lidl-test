import { ProjectLog } from './project-log';

export class DailyLog {
  id?: string;
  date: string;
  arrive?: string;
  leave?: string;
  pause?: number;
  logs: ProjectLog[] = [];
}
