import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProjectEditPageRoutingModule } from './project-edit-routing.module';

import { ProjectEditPage } from './project-edit.page';
import { AppSharedModule } from 'src/app/app.shared.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ProjectEditPageRoutingModule, AppSharedModule],
  declarations: [ProjectEditPage],
})
export class ProjectEditPageModule {}
