import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Project } from 'src/app/dto/project';
import { InfoService } from 'src/app/services/info.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.page.html',
  styleUrls: ['./project-edit.page.scss'],
})
export class ProjectEditPage implements OnInit {
  project: Project = {};

  constructor(
    private route: ActivatedRoute,
    private infoService: InfoService,
    private navCtrl: NavController,
    private projectService: ProjectService
  ) {}

  ngOnInit() {
    const projectId: string = this.route.snapshot.params.id;
    this.project = this.projectService.getById(projectId);
  }

  saveProjectDetails(): void {
    this.projectService.update(this.project);
    this.infoService.showSuccess('Saved');
    this.navCtrl.back();
  }
}
