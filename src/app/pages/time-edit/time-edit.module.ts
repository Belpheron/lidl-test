import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeEditPageRoutingModule } from './time-edit-routing.module';

import { TimeEditPage } from './time-edit.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, TimeEditPageRoutingModule],
  declarations: [TimeEditPage],
})
export class TimeEditPageModule {}
