import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { InfoService } from 'src/app/services/info.service';
import { DailyLogService } from 'src/app/services/daily-log.service';
import { ProjectLog } from 'src/app/dto/project-log';

@Component({
  selector: 'app-time-edit',
  templateUrl: './time-edit.page.html',
  styleUrls: ['./time-edit.page.scss'],
})
export class TimeEditPage implements OnInit {
  projectLog: ProjectLog;
  private logId: string;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private dailyLogService: DailyLogService,
    private infoService: InfoService
  ) {}

  ngOnInit() {
    this.logId = this.route.snapshot.params.logId;
    const projectId: string = this.route.snapshot.params.projectId;
    this.projectLog = this.dailyLogService.getProjectLogById(this.logId, projectId);
  }

  onSubmit() {
    const timeToAdd: number = this.projectLog.amount;
    if (!this.isTimeValid(timeToAdd)) {
      this.infoService.handleError(new Error('Invalid time. Must be multiple of 0,5.'));
      return;
    }

    this.dailyLogService.updateProjectLog(this.logId, this.projectLog);
    this.infoService.showSuccess('Saved.');
    this.navCtrl.back();
  }

  deleteLog(): void {
    this.dailyLogService.deleteProjectLog(this.logId, this.projectLog.id);
    this.infoService.showWarning(`Log deleted.`);
    this.navCtrl.back();
  }

  /**
   *
   * @param time Checks if the given time is valid or not. A valid time must be multiple of 0.5 and greater than 0
   * @returns true if the time is valid. false elsewhere
   */
  private isTimeValid(time: number): boolean {
    return time % 0.5 === 0 && time > 0;
  }
}
