import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeEditPage } from './time-edit.page';

const routes: Routes = [
  {
    path: '',
    component: TimeEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeEditPageRoutingModule {}
