import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DailyViewPageRoutingModule } from './daily-view-routing.module';
import { DailyViewPage } from './daily-view.page';
import { AppSharedModule } from 'src/app/app.shared.module';
import { DirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, DailyViewPageRoutingModule, AppSharedModule, DirectivesModule],
  declarations: [DailyViewPage],
})
export class DailyViewPageModule {}
