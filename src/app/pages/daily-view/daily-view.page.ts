import { Component, OnInit } from '@angular/core';
import { DailyFormModel } from 'src/app/dto/daily-form-model';
import { Project } from 'src/app/dto/project';
import { InfoService } from 'src/app/services/info.service';
import { DateTime } from 'luxon';
import { DailyLog } from 'src/app/dto/daily-log';
import { ProjectService } from 'src/app/services/project.service';
import { DailyLogService } from 'src/app/services/daily-log.service';
import { ModalController, NavController } from '@ionic/angular';
import { ProjectLog } from 'src/app/dto/project-log';
import { Settings } from 'src/app/dto/settings';
import { SettingsService } from 'src/app/services/settings.service';
import { Setting } from 'src/app/dto/setting';
import { ProjectModalComponent } from 'src/app/components/project-modal/project-modal.component';

@Component({
  selector: 'app-daily-view',
  templateUrl: './daily-view.page.html',
  styleUrls: ['./daily-view.page.scss'],
})
export class DailyViewPage implements OnInit {
  model: DailyFormModel = {};
  projects: Project[] = [];
  showAll = false;
  dailyLog: DailyLog;
  settings: Settings;
  private dateFormat = 'dd.MM.yyyy';

  constructor(
    private infoService: InfoService,
    private projectService: ProjectService,
    private dailyLogService: DailyLogService,
    private settingsService: SettingsService,
    private navCtrl: NavController,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    const date: string = DateTime.now().toFormat(this.dateFormat);
    this.settings = this.settingsService.getSettings();
    this.dailyLog = this.dailyLogService.getByDate(date) || { date, logs: [] };
    this.model.selectedDate = date;
    this.loadProjects();
    this.mapDefaultSettings();
  }

  onSubmit(): void {
    const timeToAdd: number = this.model.timeToAdd;

    if (!this.isTimeValid(timeToAdd)) {
      this.infoService.handleError(new Error('Invalid time. Must be multiple of 0,5.'));
      this.model.timeToAdd = null;
      return;
    }

    if (!this.model.selectedProject) {
      this.infoService.handleError(new Error('Select a project to add time.'));
      return;
    }

    const updatedLog: DailyLog = this.dailyLogService.addProjectLog(this.model.selectedDate, this.model.selectedProject.id, timeToAdd);

    this.dailyLog.logs = updatedLog.logs;
    this.dailyLog.id = updatedLog.id;

    this.infoService.showSuccess('Added.');
    this.model.timeToAdd = null;
  }

  /**
   * Redirects to the edit time screen
   * @param projectLog the project object
   */
  editProjectLog(projectLog: ProjectLog): void {
    this.navCtrl.navigateForward([`daily/logs/${this.dailyLog.id}/project-logs/${projectLog.id}`]);
  }

  /**
   * Sums a day to the selected date and the fetches the daily log for that date
   */
  dayForward(): void {
    const newDate: string = DateTime.fromFormat(this.model.selectedDate, this.dateFormat).plus({ days: 1 }).toFormat(this.dateFormat);
    this.changeDay(newDate);
  }

  /**
   * Substracts a day from the selected date and then fetches the daily fog the that date
   */
  dayBackwards(): void {
    const newDate: string = DateTime.fromFormat(this.model.selectedDate, this.dateFormat).minus({ days: 1 }).toFormat(this.dateFormat);
    this.changeDay(newDate);
  }

  updateDailyLog(): void {
    this.dailyLogService.updateDailyLog(this.model.selectedDate, this.dailyLog);
  }

  /**
   * Sums the hours for all the logs for the current day
   * @returns the sum of all hours for the current day
   */
  getTotalLoggedTime(): number {
    let total = 0;
    if (this.dailyLog && this.dailyLog.logs) {
      this.dailyLog.logs.forEach((log) => (total += log.amount));
    }

    return total;
  }

  /**
   * Opens a modal that will allow the user to select a project from a list
   * @returns unlocks the flow once the modal is closed
   */
  async openProjectModal() {
    const modal: HTMLIonModalElement = await this.modalController.create({
      component: ProjectModalComponent,
      cssClass: 'projects-modal-wrapper',
      componentProps: {
        showAll: this.showAll,
        selectedProject: this.model.selectedProject,
        projects: this.projects,
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    const { showAll, selectedProject, projects } = data;
    this.showAll = showAll;
    this.projects = projects;
    this.model.selectedProject = selectedProject;
  }

  /**
   * Calculates the time left for the current daily log. This is calculated using the arrive, leave and pause times, an also
   * the current project logs. If no arrive or leave times are set, this value won't be calculated
   * @returns time left for current daily log
   */
  getTimeLeft(): string {
    if (!this.dailyLog.arrive || !this.dailyLog.leave) {
      return '--';
    }
    const leave: DateTime = DateTime.fromISO(this.dailyLog.leave);
    const arrive: DateTime = DateTime.fromISO(this.dailyLog.arrive);

    const diff: number = leave.diff(arrive, ['hours']).hours - (this.dailyLog.pause || 0);

    const timeLeft: number =
      this.dailyLog.logs.length === 0 ? diff : diff - this.dailyLog.logs.reduce((a, c) => ({ ...a, amount: a.amount + c.amount })).amount;
    const roundedTimeLeft: number = Math.round(timeLeft * 10) / 10;

    return `${roundedTimeLeft === 0 ? '' : roundedTimeLeft > 0 ? '- ' : '+ '}${Math.abs(roundedTimeLeft)} h`;
  }

  ionViewDidEnter() {
    this.changeDay(this.model.selectedDate);
  }

  /**
   * Loads the projects to be shown in the project combo. Takes into account the option 'showAll'. If
   * it's false, then loads only projects for this week and the previous one. Loads all projects if false.
   */
  private loadProjects(): void {
    if (this.showAll) {
      return;
    }

    const currentDate: DateTime = DateTime.fromFormat(this.model.selectedDate, this.dateFormat);
    const end = currentDate.endOf('week');
    const start = currentDate.minus({ weeks: 1 }).startOf('week');

    this.projects = this.projectService.getByRange(start, end);
  }

  /**
   * Changes the selected day to the given date, then loads the daily log for that day
   * @param date the target date
   */
  private changeDay(date: string): void {
    this.model.selectedDate = date;
    this.dailyLog = this.dailyLogService.getByDate(date) || { date, logs: [] };
    this.mapDefaultSettings();
  }

  /**
   * Maps the default time settings to the form model
   */
  private mapDefaultSettings(): void {
    const weekDayName = this.getWeekDayForDate(this.model.selectedDate);
    if (weekDayName === 'sunday' || weekDayName === 'saturday') {
      return;
    }

    const { arrive, leave, pause } = this.dailyLog;
    const defaultWeekDay: Setting = this.settings[weekDayName];
    if (!arrive && defaultWeekDay.arrive) {
      this.dailyLog.arrive = defaultWeekDay.arrive;
    }

    if (!leave && defaultWeekDay.leave) {
      this.dailyLog.leave = defaultWeekDay.leave;
    }

    if (!pause && defaultWeekDay.pause) {
      this.dailyLog.pause = defaultWeekDay.pause;
    }
  }

  /**
   * Gets the name of the week day for the selected date
   * @param date a string containing a date
   * @returns the name of the week day
   */
  private getWeekDayForDate(date: string): string {
    return DateTime.fromFormat(date, this.dateFormat).toLocaleString({ locale: 'en', weekday: 'long' }).toLowerCase();
  }

  /**
   *
   * @param time Checks if the given time is valid or not. A valid time must be multiple of 0.5 and greater than 0
   * @returns true if the time is valid. false elsewhere
   */
  private isTimeValid(time: number): boolean {
    return time % 0.5 === 0 && time > 0;
  }
}
