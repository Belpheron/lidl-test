import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DailyViewPage } from './daily-view.page';

const routes: Routes = [
  {
    path: '',
    component: DailyViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DailyViewPageRoutingModule {}
