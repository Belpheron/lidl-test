import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Project } from 'src/app/dto/project';
import { InfoService } from 'src/app/services/info.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.page.html',
  styleUrls: ['./projects.page.scss'],
})
export class ProjectsPage implements OnInit {
  newProjectName: string;
  projects: Project[] = [];

  constructor(private navCtrl: NavController, private projectService: ProjectService, private infoService: InfoService) {}

  ngOnInit() {
    this.projects = this.projectService.getAll();
  }

  editProject(projectId: string): void {
    this.navCtrl.navigateForward(['projects/edit', projectId]);
  }

  toggleVisibility(project: Project): void {
    const { disabled } = project;
    project.disabled = disabled ? null : true;
    this.projectService.update(project);
  }

  addNewProject(name: string): void {
    this.projectService.add({ name });
    this.infoService.showSuccess('Created.');
    this.newProjectName = '';
  }
}
