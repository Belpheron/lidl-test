import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WeeklyViewPage } from './weekly-view.page';

const routes: Routes = [
  {
    path: '',
    component: WeeklyViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WeeklyViewPageRoutingModule {}
