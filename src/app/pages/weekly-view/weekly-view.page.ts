import { Component, OnInit } from '@angular/core';
import { DateTime } from 'luxon';
import { WeeklyLog } from 'src/app/dto/weekly-log';
import { DailyLogService } from 'src/app/services/daily-log.service';

@Component({
  selector: 'app-weekly-view',
  templateUrl: './weekly-view.page.html',
  styleUrls: ['./weekly-view.page.scss'],
})
export class WeeklyViewPage implements OnInit {
  weeklyLogs: WeeklyLog;
  selectedDate: DateTime;

  constructor(private dailyLogService: DailyLogService) {}

  ngOnInit() {
    this.selectedDate = DateTime.now();
    this.loadWeeklyLogs();
  }

  ionViewDidEnter() {
    this.loadWeeklyLogs();
  }

  weekForward(): void {
    this.selectedDate = this.selectedDate.plus({ weeks: 1 });
    this.loadWeeklyLogs();
  }

  weekBackwards(): void {
    this.selectedDate = this.selectedDate.minus({ weeks: 1 });
    this.loadWeeklyLogs();
  }

  /**
   * Removes the year from the given date string
   * @param date the date string
   * @returns the same date, without the year part
   */
  removeYear(date: string): string {
    return date == null ? null : DateTime.fromFormat(date, 'dd.MM.yyyy').toFormat('dd.MM');
  }

  /**
   * Loads the weekly logs for the selected date
   */
  private loadWeeklyLogs(): void {
    const end = this.selectedDate.endOf('week');
    const start = this.selectedDate.startOf('week');
    this.weeklyLogs = this.dailyLogService.getWeeklyLogs(start, end);
  }
}
