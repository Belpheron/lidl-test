import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WeeklyViewPageRoutingModule } from './weekly-view-routing.module';

import { WeeklyViewPage } from './weekly-view.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WeeklyViewPageRoutingModule
  ],
  declarations: [WeeklyViewPage]
})
export class WeeklyViewPageModule {}
