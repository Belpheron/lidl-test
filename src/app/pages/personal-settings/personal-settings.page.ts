import { Component, OnInit, ViewChild } from '@angular/core';
import { Settings } from 'src/app/dto/settings';
import { SettingsService } from 'src/app/services/settings.service';
import { StoreService } from 'src/app/services/store.service';
import { saveAs } from 'file-saver';
import { FilePickerDirective, ReadFile, ReadMode } from 'ngx-file-helpers';
import { InfoService } from 'src/app/services/info.service';
import { Store } from 'src/app/dto/store';

@Component({
  selector: 'app-personal-settings',
  templateUrl: './personal-settings.page.html',
  styleUrls: ['./personal-settings.page.scss'],
})
export class PersonalSettingsPage implements OnInit {
  settings: Settings;
  readMode = ReadMode.dataURL;
  picked: ReadFile | null = null;
  status: string | null = null;

  @ViewChild('filePicker', { static: false })
  private filePicker: FilePickerDirective | null = null;

  constructor(private settingsService: SettingsService, private storeService: StoreService, private infoService: InfoService) {}

  ngOnInit() {
    this.settings = this.settingsService.getSettings();
  }

  /**
   * Export the current store to an external .json file
   */
  export(): void {
    const dailyLogs = this.storeService.getDailyLogs();
    const projects = this.storeService.getProjects();
    const settings = this.storeService.getSettings();

    const data: string = JSON.stringify({ dailyLogs, projects, settings });

    const blob = new Blob([data], { type: 'text/plain;charset=utf-8' });

    try {
      saveAs(blob, 'settings.json');
      this.infoService.showSuccess('Exported.');
    } catch (e) {
      this.infoService.handleError(e);
    }
  }

  /**
   * Listener that will be called when a new file is picked. It will try to read the file and
   * obtain a valid store. Then will ovewrite the current store with it
   * @param file the file that has been read from the file system
   */
  onFilePicked(file: ReadFile) {
    this.picked = file;
    try {
      file.underlyingFile
        ?.text()
        .then((f) => {
          const store: Store = JSON.parse(f);

          if (store == null) {
            this.infoService.handleError('Could not find a valid file.');
          }

          this.storeService.setStore(store);
          this.infoService.showSuccess('New store saved.');
        })
        .catch((e) => {
          this.infoService.handleError(e);
        });
    } catch (e) {
      this.infoService.handleError(e);
    }
  }

  /**
   * Listener that fires once the file has been read. It resets he file picker
   * @param fileCount the number of files read
   */
  onReadEnd(fileCount: number) {
    if (this.filePicker !== null) {
      this.filePicker.reset();
    }
  }
}
