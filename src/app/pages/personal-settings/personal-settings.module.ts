import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonalSettingsPageRoutingModule } from './personal-settings-routing.module';

import { PersonalSettingsPage } from './personal-settings.page';
import { AppSharedModule } from 'src/app/app.shared.module';
import { NgxFileHelpersModule } from 'ngx-file-helpers';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, PersonalSettingsPageRoutingModule, AppSharedModule, NgxFileHelpersModule],
  declarations: [PersonalSettingsPage],
})
export class PersonalSettingsPageModule {}
