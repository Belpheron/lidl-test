import { Component, OnInit } from '@angular/core';
import { Page } from 'src/app/dto/page';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit {
  public pages: Page[] = [
    {
      title: 'Daily view',
      url: '/daily',
      icon: 'calendar-number-outline',
    },
    {
      title: 'Weekly view',
      url: 'daily/weeks',
      icon: 'calendar-outline',
    },
    {
      title: 'Projects',
      url: '/projects',
      icon: 'book-outline',
    },
    {
      title: 'Personal settings',
      url: 'daily/settings',
      icon: 'settings-outline',
    },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor() {}

  ngOnInit() {}
}
