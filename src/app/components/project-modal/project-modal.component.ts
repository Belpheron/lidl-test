import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Project } from 'src/app/dto/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-modal',
  templateUrl: './project-modal.component.html',
  styleUrls: ['./project-modal.component.scss'],
})
export class ProjectModalComponent implements OnInit {
  @Input() showAll: boolean;
  @Input() selectedProject: Project;
  @Input() projects: Project[];

  constructor(private projectService: ProjectService, private modalController: ModalController) {}

  ngOnInit() {}

  showAllProjects(): void {
    this.showAll = true;
    this.projects = this.projectService.getAll();
  }

  dismissModal(): void {
    this.modalController.dismiss({
      showAll: this.showAll,
      selectedProject: this.selectedProject,
      projects: this.projects,
    });
  }

  /**
   * Marks the given project as selected and then closes the modal
   * @param project the project that will be selected
   */
  selectProject(project: Project): void {
    this.selectedProject = project;
    this.dismissModal();
  }
}
