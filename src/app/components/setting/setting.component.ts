import { Component, Input, OnInit } from '@angular/core';
import { Setting } from 'src/app/dto/setting';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent {
  @Input() setting: Setting;

  constructor(private settingsService: SettingsService) {}

  updateSetting(): void {
    this.settingsService.updateSetting(this.setting);
  }
}
