import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appValidTime]',
})
export class ValidTimeDirective {
  constructor(public el: ElementRef) {}

  @HostListener('change') onChange() {
    const element = this.el.nativeElement;

    try {
      const numericValue: number = element.value;
      if (numericValue % 0.5 === 0) {
        return;
      } else {
        this.el.nativeElement.value = '';
      }
    } catch (e) {
      this.el.nativeElement.value = '';
    }
  }
}
