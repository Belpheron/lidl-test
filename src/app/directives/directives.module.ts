import { NgModule } from '@angular/core';
import { ValidTimeDirective } from './valid-time.directive';

@NgModule({
  declarations: [ValidTimeDirective],
  exports: [ValidTimeDirective],
})
export class DirectivesModule {}
