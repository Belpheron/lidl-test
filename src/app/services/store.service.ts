import { Injectable } from '@angular/core';
import { DailyLog } from '../dto/daily-log';
import { Project } from '../dto/project';
import { Store } from '../dto/store';
import { generate } from 'shortid';
import { Settings } from '../dto/settings';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  private store: Store = {
    dailyLogs: [],
    projects: [
      { id: generate(), name: 'Project 1' },
      { id: generate(), name: 'Project 2', disabled: true },
      { id: generate(), name: 'Project 3' },
    ],
    settings: {
      monday: { day: 'Monday' },
      tuesday: { day: 'Tuesday' },
      wednesday: { day: 'Wednesday' },
      thursday: { day: 'Thursday' },
      friday: { day: 'Friday' },
    },
  };

  constructor() {}

  setStore(store: Store): void {
    this.store = store;
  }

  getSettings(): Settings {
    return this.store.settings;
  }

  setSettings(settings: Settings): void {
    this.store.settings = settings;
  }

  getDailyLogs(): DailyLog[] {
    return this.store.dailyLogs;
  }

  getProjects(): Project[] {
    return this.store.projects;
  }
}
