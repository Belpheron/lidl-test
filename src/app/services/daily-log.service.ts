import { Injectable } from '@angular/core';
import { DateTime } from 'luxon';
import { generate } from 'shortid';
import { DailyLog } from '../dto/daily-log';
import { ProjectLog } from '../dto/project-log';
import { WeeklyLog } from '../dto/weekly-log';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root',
})
export class DailyLogService {
  constructor(private store: StoreService) {}

  getByDate(date: string): DailyLog {
    return this.store.getDailyLogs().find((dLog) => dLog.date === date);
  }

  getById(id: string): DailyLog {
    return this.store.getDailyLogs().find((dLgog) => dLgog.id === id);
  }

  getProjectLogs(dailyLogId: string): ProjectLog[] {
    return this.getById(dailyLogId).logs;
  }

  getProjectLogById(id: string, projectId: string): ProjectLog {
    return this.store
      .getDailyLogs()
      .find((dLog) => dLog.id === id)
      .logs.find((pLog) => pLog.id === projectId);
  }

  updateProjectLog(logId: string, projectLog: ProjectLog): void {
    const { amount } = projectLog;
    this.store
      .getDailyLogs()
      .find((dLog) => dLog.id === logId)
      .logs.map((pLog) => (pLog.id !== projectLog.id ? pLog : { ...pLog, amount }));
  }

  updateDailyLog(date: string, dailyLog: DailyLog): void {
    const dailyLogExists: boolean = this.getByDate(date) != null;
    const { arrive, leave, pause } = dailyLog;
    if (!dailyLogExists) {
      this.store.getDailyLogs().push({ date, arrive, leave, pause, logs: [], id: generate() });
      return;
    }
    this.store.getDailyLogs().map((dLog) => (dLog.date !== date ? dLog : { ...dLog, arrive, leave, pause }));
  }

  /**
   * Adds a new project log with the given data, but takes into account some considerations:
   * - if a daily log with that date doesn't exist, then creates a new daily log with a new project log
   * - if a daily log with that already exists, then checks if the project log exists. If true, updates
   * the existing project log, if false creates a new one
   * @param date the daily log date
   * @param projectId the project id for the project log
   * @param amount the amount of hours to log in the log
   * @returns the updated daily log
   */
  addProjectLog(date: string, projectId: string, amount: number): DailyLog {
    const existingDailyLog: DailyLog = this.getByDate(date);

    if (!existingDailyLog) {
      const projectName: string = this.store.getProjects().find((p) => p.id === projectId).name || '';
      const logs: ProjectLog[] = [{ id: generate(), projectId, projectName, amount }];

      return this.addDailyLog({ date, logs });
    }

    const existingProjectLog: ProjectLog = existingDailyLog.logs.find((p) => p.projectId === projectId);

    if (!existingProjectLog) {
      const projectName: string = this.store.getProjects().find((p) => p.id === projectId).name || '';
      const newLogs: ProjectLog[] = [...existingDailyLog.logs, { id: generate(), projectId, projectName, amount }];

      this.getByDate(date).logs = newLogs;

      return { ...existingDailyLog, logs: newLogs };
    } else {
      const newLogs: ProjectLog[] = this.getByDate(date).logs.map((p) =>
        p.projectId !== projectId ? p : { ...p, amount: (p.amount += amount) }
      );

      this.getByDate(date).logs = newLogs;

      return { ...existingDailyLog, logs: newLogs };
    }
  }

  /**
   * Finds all the logs for the given start and end dates, then returns them in a single
   * weekly log object. Also calculates the sum of all times and avoids duplicated projects
   * @param startDate the start date to filter
   * @param endDate the end date to filter
   * @returns an object containing weekly log information
   */
  getWeeklyLogs(startDate: DateTime, endDate: DateTime): WeeklyLog {
    const format = 'dd.MM.yyyy';
    const start: string = startDate.toFormat(format);
    const end: string = endDate.toFormat(format);

    const dLogs = this.store.getDailyLogs().filter((dLog) => {
      const compare = DateTime.fromFormat(dLog.date, format);
      return compare >= startDate && compare <= endDate;
    });

    let allPLogs: ProjectLog[] = [];
    dLogs.forEach((dLog) => {
      allPLogs = allPLogs.concat(dLog.logs);
    });

    const logs: ProjectLog[] = [];
    let sum = 0;
    allPLogs.forEach((pLog) => {
      sum += pLog.amount;

      if (logs.find((u) => u.projectName === pLog.projectName) != null) {
        logs.find((u) => u.projectName === pLog.projectName).amount += pLog.amount;
      } else {
        logs.push(pLog);
      }
    });

    return { start, end, logs, sum };
  }

  deleteProjectLog(logId: string, projectLogId: string): void {
    const updatedProjectLogs: ProjectLog[] = this.store
      .getDailyLogs()
      .find((dLog) => dLog.id === logId)
      .logs.filter((pLog) => pLog.id !== projectLogId);
    this.store.getDailyLogs().find((dLog) => dLog.id === logId).logs = updatedProjectLogs;
  }

  /**
   *
   * @param date Creates a new daily log registry for the given date and adds a new project log to it
   * @param projectId the project id
   * @param amount the amount of time to add to the project log
   * @returns the new daily log
   */
  private addDailyLog(dailyLog: DailyLog): DailyLog {
    const newLog: DailyLog = { ...dailyLog, id: generate() };
    this.store.getDailyLogs().push(newLog);
    return newLog;
  }
}
