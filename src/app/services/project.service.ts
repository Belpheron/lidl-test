import { Injectable } from '@angular/core';
import { Project } from '../dto/project';
import { generate } from 'shortid';
import { StoreService } from './store.service';
import { DateTime } from 'luxon';

@Injectable({
  providedIn: 'root',
})
export class ProjectService {
  constructor(private store: StoreService) {}

  getAll(): Project[] {
    return this.store.getProjects();
  }

  getById(id: string): Project {
    return this.store.getProjects().find((p) => p.id === id);
  }

  getByIds(ids: string[]): Project[] {
    return this.store.getProjects().filter((p) => ids.includes(p.id));
  }

  add(project: Project): void {
    this.store.getProjects().push({ ...project, id: generate() });
  }

  update(project: Project): void {
    const { name, toolName, disabled } = project;
    this.store.getProjects().map((p) => (p.id !== project.id ? p : { ...p, name, toolName, disabled }));
  }

  toggleVisibility(id: string): void {
    this.store.getProjects().map((p) => (p.id !== id ? p : { ...p, disabled: p.disabled ? null : true }));
  }

  /**
   * Gets a list of projects, filtered by start and end dates. These dates mean the first time and
   * the last time that some hours has been logged onto that project
   * @param start the start date to filter
   * @param end the end date to filter
   * @returns a list of projects
   */
  getByRange(start: DateTime, end: DateTime): Project[] {
    const ids: string[] = [];
    this.store
      .getDailyLogs()
      .filter((dLog) => {
        const compare = DateTime.fromFormat(dLog.date, 'dd.MM.yyyy');
        return compare >= start && compare <= end;
      })
      .forEach((dLog) => dLog.logs.forEach((pLog) => ids.push(pLog.projectId)));
    return this.getByIds(ids);
  }
}
