import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Logger, LoggingService } from 'ionic-logging-service';

@Injectable({
  providedIn: 'root',
})
export class InfoService {
  private logger: Logger;

  constructor(private loggingService: LoggingService, public toastController: ToastController) {
    this.logger = this.loggingService.getLogger('lidl-test.ErrorService');
  }

  handleError(error?: any): void {
    this.logger.error(`Error: ${error || 'Internal error'}`);
    const message: string = error && error instanceof Error ? error.message : error ? error : 'Internal error';
    this.showToast(message, 'danger');
  }

  showWarning(message: string): void {
    this.showToast(message, 'warning');
  }

  showSuccess(message: string): void {
    this.showToast(message, 'success');
  }

  /**
   * Shows a default toast on screen with the given message and color
   * @param message the message to show in the toast
   * @param color the toast color
   */
  private showToast(message: string, color: string): void {
    this.toastController
      .create({
        message,
        duration: 2500,
        animated: true,
        color,
      })
      .then((toast) => toast.present());
  }
}
