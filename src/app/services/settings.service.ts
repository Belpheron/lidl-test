import { Injectable } from '@angular/core';
import { Setting } from '../dto/setting';
import { Settings } from '../dto/settings';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  constructor(private store: StoreService) {}

  getSettings(): Settings {
    return this.store.getSettings();
  }

  updateSetting(setting: Setting): void {
    this.store.getSettings()[Object.keys(setting)[0]] = { ...setting };
  }

  updateSettings(settings: Settings): void {
    this.store.setSettings(settings);
  }
}
