# README

This is the application I created for the Lidl test.
Please take into consideration that I haven't used Angular for the past two years, and las version I used was the 4.

### Who is this code addressed to?

- To the recruitment team of Lidl

### Who did this code?

This was done by

- Juan Montemayor Escudero
- +34 619 107 152
- montemayor_bdt@hotmail.com

### What could be improved in the app ?

I am aware that the application could be improved in several ways, but then more time is needed.

- Add a backend server
- Add unit testing
- Add observables
- Better handle of errors

### How can I run the app?

- clone the repository
- execute 'npm run start'. This script executes 'npm install && npx ionic serve'
- set the browser in 'mobile' mode
